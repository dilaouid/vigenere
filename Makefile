# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/31 11:07:06 by dlaouid           #+#    #+#              #
#    Updated: 2019/01/26 11:46:19 by dilaouid         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

PATH_LIB =	src/lib/
PATH_NLIB =	src/nlib/
FLAG =	-Wall -Wextra -Werror -fstack-protector -g -O0 -std=c99
OPTION =	ulimit -c unlimited
SRC =	$(PATH_NLIB)ft_callflag.c $(PATH_NLIB)ft_createkey.c $(PATH_NLIB)ft_file.c $(PATH_NLIB)ft_man.c $(PATH_NLIB)ft_code.c $(PATH_NLIB)ft_extensionchecker.c $(PATH_NLIB)ft_lenUPPER.c $(PATH_LIB)ft_memcpy.c $(PATH_LIB)ft_putstr_fd.c $(PATH_LIB)ft_strjoin.c $(PATH_LIB)get_next_line.c $(PATH_LIB)ft_memmove.c $(PATH_LIB)ft_strcapitalize.c $(PATH_LIB)ft_strnlen.c $(PATH_LIB)ft_strndup.c $(PATH_LIB)tab_to_str.c $(PATH_LIB)ft_putchar.c $(PATH_LIB)ft_strchr.c $(PATH_LIB)ft_putendl.c $(PATH_LIB)ft_strdel.c $(PATH_LIB)ft_strnew.c $(PATH_LIB)ft_putstr.c $(PATH_LIB)ft_strdup.c $(PATH_LIB)ft_strlen.c $(PATH_LIB)ft_strcmp.c main.c

all:
	gcc -o vigenere $(FLAG)	$(SRC) && rm -rf vigenere.dSYM

clean:
	rm -f $(PATH_NLIB)*.o $(PATH_LIB)*.o
