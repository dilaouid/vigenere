/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_extensionchecker.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 11:35:08 by dlaouid           #+#    #+#             */
/*   Updated: 2019/01/25 07:39:02 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"

int		ft_extensionchecker(char *filename)
{
	int i;

	i = 0;
	while (filename[i])
		i++;
	while (filename[i] != '.')
	{
		i--;
		if (i == 0)
			return (0);
	}
	if (filename[i] == '.' && filename[i + 1] == 'v' && filename[i + 2] == 'g'
		&& filename[i + 3] == 'e' && filename[i + 4] == '\0')
		return (1);
	else
		return (0);
}
