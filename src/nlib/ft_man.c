/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_man.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 10:54:41 by dlaouid           #+#    #+#             */
/*   Updated: 2019/01/26 11:44:18 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"

void	ft_header()
{
	ft_putstr("\e[1;1H\e[2J");
	ft_putstr("#####################################################################################################\n"COLOR_GREEN);
	ft_putstr("/////////////////////////////////////////////////////////////////////////////////////////////////////\n"COLOR_CYAN);
	ft_putstr("The Vigenere cipher is a method of encrypting alphabetic text by using a series of interwoven Caesar\nciphers, based on the letters of a keyword. It is a form of polyalphabetic substitution. The\ncipher is easy to understand and implement, but it resisted all attempts to break it for three\ncenturies, which earned it the description le chiffre indéchiffrable (French for 'the indecipherable\n cipher'). Many people have tried to implement encryption schemes that are essentially\nVigenère ciphers\n");
	ft_putstr(COLOR_GREEN"/////////////////////////////////////////////////////////////////////////////////////////////////////\n"COLOR_RESET);
	ft_putstr("#####################################################################################################\n");
	ft_putstr("\n");
}

void	ft_noarg()
{
	ft_header();
	ft_putstr(COLOR_RED"-----------------------------------------------------------------------------------------------------\nPlease call the flag -m for reading the man\n-----------------------------------------------------------------------------------------------------\n\n"COLOR_RESET);
	ft_putchar('\n');
}

void	ft_rtfm()
{
	ft_header();
	ft_putstr(COLOR_RED"-----------------------------------------------------------------------------------------------------\nYou are reading the manuel of the Vigenere Code made by dilaouid\n-----------------------------------------------------------------------------------------------------\nIf you want to encrypt a text, launch the program this way:\n./program_name 'your message' 'your key'\n-----------------------------------------------------------------------------------------------------\nFor decrypt a text with a specific key, launch the program this way :\n./program_name -d 'the mysterious text' 'the unique key'\n-----------------------------------------------------------------------------------------------------\nIf you want to encrypt a text in a specific file, launch the program this way:\n./program_name -f 'your file' 'your key'\n-----------------------------------------------------------------------------------------------------\nIf you want to decrypt a .vge file, launch the program like this :\n./program_name -df 'your .vge file' 'the unique key'\n-----------------------------------------------------------------------------------------------------\n\n"COLOR_RESET);
	ft_putchar('\n');
}

void	ft_instrucode(char *cmsg, char *msg, char *ckey)
{
	ft_header();
	ft_putstr(COLOR_RED"-----------------------------------------------------------------------------------------------------\nYou are using the encryption method.\nPlease use the -d flag for decrypt a message with your given key.\n-----------------------------------------------------------------------------------------------------\n\n"COLOR_RESET);
	ft_putstr("YOUR MESSAGE IS : "COLOR_BLUE);
	ft_putstr(cmsg);
	ft_putchar('\n');
	ft_putstr(COLOR_RESET"YOUR KEY IS : "COLOR_BLUE);
	ft_putstr(ckey);
	ft_putstr("\n\n");
	ft_putstr(COLOR_RESET"YOUR CRYPTED MESSAGE IS : "COLOR_YELLOW);
	ft_putstr(msg);
	ft_putchar('\n');
}

void	ft_instrudecode(char *cmsg, char *msg, char *ckey)
{
	ft_header();
	ft_putstr(COLOR_RED"-----------------------------------------------------------------------------------------------------\nYou are using the decryption method.\nPlease remove the -d flag for encrypt a message with a choosen key.\n-----------------------------------------------------------------------------------------------------\n\n"COLOR_RESET);
	ft_putstr("THE ENCRYPTED MESSAGE IS : "COLOR_BLUE);
	ft_putstr(cmsg);
	ft_putchar('\n');
	ft_putstr(COLOR_RESET"THE KEY IS : "COLOR_BLUE);
	ft_putstr(ckey);
	ft_putstr("\n\n");
	ft_putstr(COLOR_RESET"THE DECRYPTED MESSAGE IS : "COLOR_YELLOW);
	ft_putstr(msg);
	ft_putchar('\n');
}
