/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_code.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 10:54:22 by dlaouid           #+#    #+#             */
/*   Updated: 2019/02/04 12:32:48 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"

char	*ft_code(char *msg, char *key, int fl, int fileused)
{
	char *ckey;
	char *cmsg;
	int i = 0;
	int j = 0;
	
	ckey = ft_createkey(key);
	cmsg = ft_strdup(msg);
	if (fl == 0)
	{
		while (cmsg[j])
		{
			if (cmsg[j])
				cmsg[j] += ckey[i] + 5;
			if (fileused == 0)
			{
				while ((cmsg[j] < 'A' || cmsg[j] > 'Z') && (cmsg[j] < 'a' || cmsg[j] > 'z'))
					cmsg[j] += 26;
			}
			i++;
			if (ckey[i] == '\0')
				i = 0;
			j++;
		}
		if (fileused == 0)
			ft_instrucode(msg, cmsg, ckey);
	}
	else
	{
		while(cmsg[j])
		{
			if (cmsg[j])
				cmsg[j] -= ckey[i] + 5;
			if (fileused == 0)
			{
				while ((cmsg[j] < 'A' || cmsg[j] > 'Z') && (cmsg[j] < 'a' || cmsg[j] > 'z'))
					cmsg[j] -= 26;
			}
			i++;
			if (ckey[i] == '\0')
				i = 0;
			j++;
		}
		if (fileused == 0)
			ft_instrudecode(msg, cmsg, ckey);
	}
	return (cmsg);
}
