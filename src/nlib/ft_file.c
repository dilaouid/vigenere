/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dilaouid <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 20:05:05 by dilaouid          #+#    #+#             */
/*   Updated: 2019/01/25 08:48:01 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"

char	*ft_decrypted_flag(char *str)
{
	char *output;

	output = ft_strndup(str, (ft_strlen(str) - 4));
	return (ft_strjoin(output, ".vgd"));
}

int		ft_get_textfile(char *file, char *key, int decryptor)
{
	int fd;
	char *filename;
	char *linefd;
	char *output;
	int y = 0;

	ft_header();
	if (ft_extensionchecker(file) == 0 && decryptor == 1)
	{
		ft_putendl("\n----------");
		ft_putendl(COLOR_RED "FILE CREATION FAILED" COLOR_RESET);
		ft_putendl("----------");
		ft_putstr(file);
		ft_putendl(" isn't a correct file. Please use a .vge file.");
		return (0);
	}
	fd = open(file, O_RDONLY);
	if (fd < 0)
		return (write(2, "The file doesn't exist.\n", 24));
	close(fd);
	fd = open(file, O_RDONLY);
	while (get_next_line(fd, &linefd) > 0)
	{
		linefd = ft_strjoin(linefd, "\n");
		if (y == 0)
			output = ft_strdup(linefd);
		else
			output = ft_strjoin(output, linefd);
		y++;
		free(linefd);
	}
	close(fd);
	if (fd == -1)
		return (write(2, "For unknown reason, the file can't be closed !\n", 49));
	filename = ft_strdup(file);
	if (!decryptor)
		filename = ft_strjoin(filename, ".vge");
	else
		filename = ft_decrypted_flag(filename);
	fd = open(filename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd < 0)
		return (write(2, "The file doesn't exist.\n", 23));
	ft_putstr_fd(fd, ft_code(output, key, decryptor, 1));
	close(fd);
	if (fd < 0)
		return (write(2, "For unknown reason, the file can't be closed !\n", 46));
	ft_putendl("\n----------");
	ft_putendl(COLOR_GREEN "FILE CREATION SUCCESS" COLOR_RESET);
	ft_putendl("----------");
	ft_putstr(COLOR_GREEN "FILENAME: " COLOR_RESET);
	ft_putendl(filename);
	ft_putstr(COLOR_GREEN "KEY USED : " COLOR_RESET);
	ft_putstr(key);
	ft_putstr("\n\n");
	return (1);
}
