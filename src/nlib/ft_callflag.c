/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_callflag.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 10:54:15 by dlaouid           #+#    #+#             */
/*   Updated: 2019/01/26 11:51:34 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"

int		ft_callflag(char *str)
{
	if (ft_strcmp("-d", str) == 0)
		return (1);
	else if (ft_strcmp("-m", str) == 0)
		return (2);
	else if (ft_strcmp("-f", str) == 0)
		return (3);
	else if (ft_strcmp("-fd", str) == 0)
		return (4);
	else
		return (0);
}