/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_createkey.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 10:54:29 by dlaouid           #+#    #+#             */
/*   Updated: 2019/01/25 08:48:36 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"

char	*ft_createkey(char *key)
{
	char *ckey;
	int i;
	int j;
	
	ckey = (char *)malloc(sizeof(char) * ft_lenUPPER(key) + 1);
	if (ckey == NULL)
		return (NULL);
	i = 0;
	j = 0;
	while (key[i])
			ckey[j++] = key[i++];
	ckey[j] = '\0';
	return (ckey);
}
