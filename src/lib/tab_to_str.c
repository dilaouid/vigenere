/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dilaouid <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 15:58:26 by dilaouid          #+#    #+#             */
/*   Updated: 2018/12/22 13:35:06 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"
#include <stdio.h>
char		*tab_to_str(char **tab)
{
	char	*dest;
	int		total_length;
	int		y;

	total_length = 0;
	y = 0;
	while (tab[y + 1])
	{
		total_length += ft_strlen(tab[y]);
		printf("total: %d\n", ft_strlen(tab[y]));
		y++;
	}
	dest = (char *)malloc(sizeof(char) * total_length + 1);
	if (!dest)
		return (NULL);

	y = 0;
	while (tab[y + 1])
		dest = ft_strjoin(dest, tab[y++]);
	return (dest);
}