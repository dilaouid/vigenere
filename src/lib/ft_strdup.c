/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 10:53:58 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/31 10:53:58 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_h.h"

char	*ft_strdup(char *msg)
{
	char *cmsg;
	int i;

	cmsg = (char *)malloc(sizeof(char) * ft_strlen(msg) + 1);
	if (cmsg == NULL)
		return (NULL);
	i = 0;
	while (msg[i])
	{
		cmsg[i] = msg[i];
		i++;
	}
	cmsg[i] = '\0';
	return (cmsg);
}