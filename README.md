Vigenere v.1.2
===================================================================================
 
 Une méthode pour chiffrer un message, beaucoup plus efficace que le code de César qui ne dispose que de 26 clés possibles.
 Avec cette méthode, il n'existe qu'une seule clé alphabétique existante : et c'est à vous de la saisir.
 
 Vous pouvez crypter une entrée utilisateur ou même un fichier. Un fichier ne peut être décrypter que s'il est en format .vge
 (Si vous l'avez crypté avec ce programme, le fichier vge a déjà été automatiquement crée dans le même répertoire que ce dernier).

 Décrypter un fichier en créer un nouveau au format vgd. Du coup, aucun des fichiers crées ne sont écrasés.
 