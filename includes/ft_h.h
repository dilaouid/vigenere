/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_h.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 10:53:14 by dlaouid           #+#    #+#             */
/*   Updated: 2019/01/26 11:49:42 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _FT_H_H
# define _FT_H_H

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_RESET "\x1b[0m"
#define BUFF_SIZE 42

void	ft_putchar(char c);
void	ft_putstr(char *str);
char 	*ft_strcapitalize(char *str);
int		ft_strlen(char *str);
char	*ft_strdup(char *msg);
void	ft_instrucode(char *cmsg, char *msg, char *ckey);
void	ft_instrudecode(char *cmsg, char *msg, char *ckey);
int		ft_lenUPPER(char *str);
char	*ft_createkey(char *key);
char	*ft_code(char *msg, char *key, int fl, int file);
int		ft_callflag(char *str);
void	ft_noarg();
void	ft_rtfm();
int 	ft_get_textfile(char *file, char *key, int decryptor);
void	ft_putendl(char *str);
void	ft_putstr_fd(int fd, char *str);
void	ft_header();
int 	ft_extensionchecker(char *filename);
char	*ft_strjoin(char const *s1, char const *s2);
void	*ft_strdel(char **as);
char	*ft_strchr(const char *s, int c);
char	*ft_strnew(size_t size);
void	*ft_memmove(void *dst, const void *src, size_t len);
void	*ft_memcpy(void *dest, const void *src, size_t n);
int		get_next_line(const int fd, char **line);
char	*ft_strndup(const char *s, size_t n);
size_t	ft_strnlen(const char *s, size_t n);
char	*tab_to_str(char **tab);
int		ft_strcmp(char *s1, char *s2);

#endif
