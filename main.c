/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 14:33:24 by dlaouid           #+#    #+#             */
/*   Updated: 2019/01/25 07:39:24 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_h.h"

int	main(int argc, char **argv)
{
	if (argc == 1)
		ft_noarg();
	else if (ft_callflag(argv[1]) == 2)
		ft_rtfm();
	else if (argc == 4 && ft_callflag(argv[1]) == 3)
		ft_get_textfile(argv[2], argv[3], 0);
	else if (argc == 4 && ft_callflag(argv[1]) == 4)
		ft_get_textfile(argv[2], argv[3], 1);
	else if (argc == 3 && ft_callflag(argv[1]) == 0)
		ft_code(argv[1], argv[2], 0, 0);
	else if (argc == 4 && ft_callflag(argv[1]) == 1)
		ft_code(argv[2], argv[3], 1, 0);
	else
		ft_putstr("SYNTAX ERROR\n");
	return (0);
}
